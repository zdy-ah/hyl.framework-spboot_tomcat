package com.control;

import java.util.Map;
import org.springframework.ui.ModelMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aop.Boy;

import hyl.core.MyFun;
import hyl.core.io.MyPath;

@Controller
public class HelloController {
	@Autowired
	private Boy boy;

	@RequestMapping("/hello")
	public String hello(ModelMap map) { // Map<String, Object>
		map.put("name", "HelloController");
		boy.buy();
		MyFun.print("路径", MyPath.getPathOfClassLoader(this.getClass().getClassLoader()));
		return "test2";
	}

	@RequestMapping("/hello2")
	public String hello2(Map<String, Object> map) { // Map<String, Object>
		map.put("name", "HelloController2");
		MyFun.print("路径", MyPath.getPathOfClassLoader(this.getClass().getClassLoader()));
		return "test2";
	}

	@RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
	public String id(@PathVariable("id") Integer id) {
		MyFun.print("id", id);
		return "test2";
	}

	/*
	 * @RequestMapping value 和params 的详解
	 * 
	 * 
	 * 如类没有定义请求映射 类方法中的value代表根路径 如果在类方法中有点类似于struts中 action的id params 为请求参数的数组
	 * 支持一些简单的表达式 params={"!id","name!=James"} 表示不能带名称为id的参数 而且name的值不能为James 等等表达式
	 * 
	 * @RequestMapping(value = "/init", params = {"id=myValue"}) 只有存在了请求参数id=myValue
	 * /init.action?id=myValue 才会被initData处理
	 * 
	 * @RequestMapping(value = "/init", params = {"name=kobe", "number=23"})
	 * /init.action?name=kobe&&number=23 否则 404错误
	 * 
	 * 一旦abc init 为占位符即用｛｝包括起来 该请求默认为下面 http://localhost:8080/abc/init.action 如果被赋值
	 * 例如 abc = "hello"; init = "world"; 则下面网址也可以访问ininData方法
	 * http://localhost:8080/hello/world.action 这形成了具有REST（表现层状态转化）风格的请求形式 表示 abc
	 * 的id为 init的实际赋值 但是请求的方法必须为GET
	 * 
	 * @RequestParam 详解 接收 请求参数 required参数默认为false 表示 可以为空 如果为 数据的基本类型 一旦没有赋值 提交
	 * 会被赋值null 抛出异常 一般推荐用包装类 来接收 比如 int 用 Integer double 用Double 等
	 */
	@RequestMapping(value = "/k/{abc}/{init}")
	public String k(@PathVariable("abc") String abc, @PathVariable("init") String init,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "age", required = false) Integer age) {
		abc = "hello";
		init = "world";
		MyFun.print("k ", abc, ",", init, ",", name, " ", age);
		System.out.println(name + age);
		return "test2";
	}

}
